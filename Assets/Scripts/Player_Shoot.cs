﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Player_Shoot : MonoBehaviour
{
    [Header("Shooting Options")]
    public float bulletSpread;                          //How large the cone of fire is
    private float initbulletSpread;                     //How large the cone of fire is
    public float ADS_Spread;                            //How large the cone of fire is
    public float bulletRange = 10.0f;                   //How far each bullet can go 
    public int bulletDamage = 45;                       //How much damage each bullet does on impact
    public int magSize = 30;                            //The max amount of bullets in a magazine
    public int maxAmmo = 150;                           //The max amount of bullets the player can carry at any given time
    public float reloadSpeed = 2.0f;                    //How long it takes to reload 
    [Tooltip("Determines amount of bullets to be fired when the player shoots")]
    public float bulletsPerShot = 1.0f;                 //How many bullets were shot at once
    [Tooltip("Time Between Shots")]
    public float fireSpeed = 1.0f;                      //Time between shots
    public ParticleSystem tracerSystem;
    
    private bool reloading = false;
    private int bulletsInMag = 0;                       //Tracks the bullets in the current magazine 
    private int ammoCount = 0;                          //Tracks the total amount of bullets the player is carrying
    private float shootTimer = 0;                       //Time it takes to shoot
    private float reloadTimer = 0;                      //Tracks how long it takes to reload

    [Header("BulletHole")]
    public Sprite[] bulletHoles;
    public GameObject bulletHoleObject;                     //When a static object is shot this image is instantiated at the point where it hits

    [Header("UI")]
    public Text bulletsText;                            //Shows the current ammo
    public Image outerReticle;                           //The outerReticle in which moves when shooting
    private Canvas canvas;
    //Change UI size everytime Shoot Gun

    private Vector3 outerReticleScaleInit;                  //Get the scale before manipulating it

    [Header("Tracers")]
    public GameObject tracerBullet;
    [Space(10)]
    public float tracerSpeed;
    public GameObject bulletOrigin;


    
    public void SetAmmoCount(int value)
    {
        ammoCount = value;
    }

    void Start()
    {
        bulletsInMag = magSize;
        ammoCount = maxAmmo;
        reloadTimer = reloadSpeed;
        bulletsText = null;// GetComponent<SetupLocalPlayer>().canvas.transform.FindChild("bulletsText").GetComponent<Text>();
        outerReticle = null;// GetComponent<SetupLocalPlayer>().canvas.transform.FindChild("outerReticle").GetComponent<Image>();
        //outerReticleScaleInit = outerReticle.transform.localScale;
        initbulletSpread = bulletSpread;

        canvas = GetComponent<Player_Health>().canvas;
    }

    void Update()
    {
        if (canvas == null)
            canvas = GetComponent<Player_Health>().canvas;
        
        if (bulletsText == null)
            bulletsText = canvas.transform.FindChild("bulletsText").GetComponent<Text>();
        if (outerReticle == null)
        {
            outerReticle = canvas.transform.FindChild("outerReticle").GetComponent<Image>();
            outerReticleScaleInit = outerReticle.transform.localScale;
        }
        //Aiming Down Sights
        if (Input.GetMouseButton(1))
            bulletSpread = ADS_Spread;
        else
            bulletSpread = initbulletSpread;

        //If the player can shoo and the player has pressed the shoot button
        if (shootTimer <= 0 && Input.GetMouseButton(0) && !reloading && bulletsInMag > 0)
        {
            //Shoot and reset the delay
            for (int i = 0; i < bulletsPerShot; ++i)
                ShootRay();
            shootTimer = fireSpeed;
        }

        if (shootTimer > 0)
            shootTimer -= Time.deltaTime;

        //Reloading
        if (bulletsInMag <= 0)
            reloading = true;
        else if (bulletsInMag < magSize && Input.GetKeyDown(KeyCode.R))
            reloading = true;

        if (reloading)
            ReloadWeapon();

        //Making the text UI change every frame
        bulletsText.text = bulletsInMag.ToString() + "/" + ammoCount.ToString();

        //Clamping the outer rings of reticle
        outerReticle.transform.localScale = new Vector3(
        Mathf.Clamp(outerReticle.transform.localScale.x, outerReticleScaleInit.x, outerReticleScaleInit.x * 1.3f),
        Mathf.Clamp(outerReticle.transform.localScale.y, outerReticleScaleInit.y, outerReticleScaleInit.y * 1.3f),
        Mathf.Clamp(outerReticle.transform.localScale.z, outerReticleScaleInit.z, outerReticleScaleInit.z * 1.3f));

        outerReticle.transform.localScale *= 0.99f;
    }

    void ShootRay()
    {
        outerReticle.transform.localScale *= 2;

        bulletsInMag -= 1;
        //ray casts will form a ring
        //float randomRadius = bulletSpread;

        // ray casts will be in a circle area
        float randomRadius = Random.Range(0, bulletSpread);

        float randomAngle = Random.Range(0, 2 * Mathf.PI);

        //Calculate raycast direction
        Vector3 direction = new Vector3(randomRadius * Mathf.Cos(randomAngle), randomRadius * Mathf.Sin(randomAngle), bulletRange);

        //make direction match the transform
        // eg. converting the vector3 forward to transform forward
        Camera cam = FindActiveCamera();

        direction = cam.transform.TransformDirection(direction.normalized);

        Debug.Log(bulletSpread);

        //raycast and debug
        Ray r = new Ray(cam.transform.position, direction);
        RaycastHit hit;

        ShootTracer(direction);

        if (Physics.Raycast(r, out hit))
        {
            //for debug only
            Debug.DrawLine(transform.position, hit.point);

            //if the object hit is an enemy
            if (hit.collider.gameObject.tag == "Enemy")
            {
                //tell the enemy to take damage depending on the players damage
                hit.collider.gameObject.GetComponent<Enemy_Health>().TakeDamage(bulletDamage / bulletsPerShot);
            }
            else if (hit.collider.gameObject.tag == "Player" && hit.collider.gameObject != this.gameObject)
            {
                GetComponent<Player_NetworkManager>().CmdDamagePlayer(hit.collider.GetComponent<Player_NetworkManager>().netId, bulletDamage);

                //syntax for networking buffs (below)
                //GetComponent<Player_NetworkManager>().CmdUpdateBuffs(hit.collider.GetComponent<Player_NetworkManager>().netId, 3, -50.0f);
            }
            else if (hit.collider.gameObject.isStatic)
            {
                CreateBulletHole(hit);
            }
        }
        else
            //for debug only
            Debug.DrawLine(transform.position, direction * 100);
    }

    void ShootTracer(Vector3 direction)
    {
        Vector3 origin = bulletOrigin.transform.position;
        GameObject tracer = Instantiate(tracerBullet, origin + direction, FindActiveCamera().transform.rotation) as GameObject;
        tracer.GetComponent<Rigidbody>().AddForce(direction * tracerSpeed);
        Destroy(tracer, 1);
    }

    void ReloadWeapon()
    {
        //if you have bullets to reload with
        if (ammoCount > 0)
        {
            //reload
            if (reloadTimer <= 0)
            {
                reloadTimer = reloadSpeed;
                ammoCount -= magSize;
                if (bulletsInMag > 0)
                    ammoCount += bulletsInMag;

                bulletsInMag = magSize;

                if (ammoCount < 0)
                {
                    bulletsInMag += ammoCount;
                    ammoCount -= ammoCount;

                }
                reloading = false;
            }
            reloadTimer -= Time.deltaTime;
        }
        else
        {
            //display message here telling them to find a new weapon
            //Debug.Log("Find a new weapon");
            reloading = false;
        }
    }

    Camera FindActiveCamera()
    {
        Camera[] cameras = FindObjectsOfType<Camera>();
        for (int i = 0; i < cameras.Length; ++i)
        {
            if (cameras[i].enabled == true && cameras[i].tag == "PlayerCamera")
                return cameras[i];

        }
        return null;
    }

    void CreateBulletHole(RaycastHit hitInfo)
    {
        var hitRotation = Quaternion.LookRotation(-hitInfo.normal);
        GameObject newBulletHole = Instantiate(bulletHoleObject, hitInfo.point, hitRotation) as GameObject;
        newBulletHole.transform.position += newBulletHole.transform.forward * -0.001f;
        newBulletHole.GetComponent<SpriteRenderer>().sprite = bulletHoles[0];
        newBulletHole.transform.parent = GameObject.Find("BulletHolesHolder").gameObject.transform;
    }
}
