﻿using UnityEngine;
using System.Collections;

public class Player_PVP : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void TakeDamage(float damage)
    {
        GetComponent<Player_Health>().TakeDamage(damage);
    }
}
