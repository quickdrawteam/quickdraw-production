﻿using UnityEngine;
using System.Collections;

public class Billboard : MonoBehaviour
{

	// Update is called once per frame
	void Update ()
    {
        Camera[] cameras = FindObjectsOfType<Camera>();
        for(int i = 0; i < cameras.Length; ++i)
        {
            if(cameras[i].enabled == true && cameras[i].tag == "PlayerCamera")
                this.transform.LookAt(cameras[i].transform.position);
            
        }
        this.transform.Rotate(new Vector3(0, 180, 0));
	}
}
