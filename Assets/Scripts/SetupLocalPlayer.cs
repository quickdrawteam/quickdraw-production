﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class SetupLocalPlayer : NetworkBehaviour
{
    [SyncVar]
    public string playerName;

    [SerializeField]
    Behaviour[] componentsToDisable;
    //to send message from server to all clients, use [syncvar]
    //to send message form client to server, use [command]
    // Use this for initialization

    void Awake()
    {
        if (isLocalPlayer)
        {
                GetComponent<Player_Health>()._isLocalPlayer = true;
        }
    }

    void Start()
    {
        //if (isLocalPlayer)
        //{
        //    canvas = Instantiate(canvas) as Canvas;
        //    canvas.enabled = true;
        //    canvas.gameObject.SetActive(true);
        //    GetComponent<Player_Movement>().enabled = true;
        //    GetComponent<Player_Shoot>().enabled = true;
        //    transform.GetChild(0).GetComponent<Camera>().enabled = true;
        //    this.GetComponentInChildren<TextMesh>().text = playerName;
        //}
        if (!isLocalPlayer)
        {
            for (int i = 0; i < componentsToDisable.Length; ++i)
            {
                componentsToDisable[i].enabled = false;
            }
        }
        else
        {

        }
    }

    void Update()
    { 
        this.GetComponentInChildren<TextMesh>().text = playerName;
    }


    public bool CheckIfLocalPlayer()
    {
        return isLocalPlayer;
    }
}
