﻿using UnityEngine;
using System.Collections;


public class Player_Movement : MonoBehaviour
{
    [Header("Player Movement")]
    public int moveSpeed = 10;                   // rate at which the player moves
    public float runMultiplier = 2.0f;                 // multiplier that alters move speed when running
    public float jumpForce = 1.0f;                 // how high the player jumps
    public float groundCheckDistance = 0.001f;               // distance used for checking if player is grounded (0.01 works best)
    public float stickToGroundDistance = 0.5f;                 // stops the player
    public float capsuleOffset = 0.01f;                // reduces capsule radius by value to avoid getting stuck in walls
    public KeyCode runKey = KeyCode.LeftShift;    // key press to start running

    private bool isRunning = false;                // bool used to check if the player is running
    private bool jumping = false;                // bool used to determine whether the player is jumping
    private bool jump = false;                // bool used to check whether the player should jump
    private bool addedForce = false;                // bool used to prevent double jumps
    private bool isGrounded = true;                 // bool used to check if the player is in the air
    private bool previouslyGrounded = false;                // bool used to check if the player was grounded last frame
    private float currentSpeed = 0.0f;                 // used to move the player
    private Rigidbody rigidbody = null;                 // player's rigidbody (used for moving)
    private CapsuleCollider capsule = null;                 // player's collider (used for collision detection)
    private Vector3 groundContactNormal = Vector3.zero;         // used to determine correct action when jumping

    [Header("Camera")]
    public float horizontalSpeed;
    public float verticalSpeed;

    private float yaw;
    private float pitch;

    // Use this for initialization
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        capsule = GetComponent<CapsuleCollider>();
        currentSpeed = moveSpeed;
    }


    void FixedUpdate()
    {
        GetInput();
    }

    void GetInput()
    {
        GroundCheck();

        #region Movement
        if (Input.GetKey(KeyCode.W))
            transform.position += transform.forward * Time.deltaTime * moveSpeed;

        if (Input.GetKey(KeyCode.A))
            transform.position -= transform.right * Time.deltaTime * moveSpeed;

        if (Input.GetKey(KeyCode.S))
            transform.position -= transform.forward * Time.deltaTime * moveSpeed;

        if (Input.GetKey(KeyCode.D))
            transform.position += transform.right * Time.deltaTime * moveSpeed;
        #endregion

        #region Jumping
        if (Input.GetKey(KeyCode.Space))
            jump = true;

        if (isGrounded)
        {
            rigidbody.drag = 1.0f;
            if (jump)
            {
                rigidbody.drag = 1.0f;
                rigidbody.velocity = new Vector3(rigidbody.velocity.x, 0.0f, rigidbody.velocity.z);
                rigidbody.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
                addedForce = true;
                jumping = true;
            }

            // if (!jumping && Mathf.Abs(x) < float.Epsilon && Mathf.Abs(z) < float.Epsilon && rigidbody.velocity.magnitude < 1)
            //     rigidbody.Sleep();
        }
        else
        {
            rigidbody.drag = 1.0f;

            if (previouslyGrounded && !jumping)
            {
                StickToGround();
            }
        }

        jump = false;
        #endregion

        #region Running
        if (Input.GetKey(runKey))
            currentSpeed = moveSpeed * runMultiplier;
        else
            currentSpeed = moveSpeed;
        #endregion
    }

    void StickToGround()
    {
        RaycastHit hit;
        if (Physics.SphereCast(transform.position, capsule.radius * (1.0f - capsuleOffset), Vector3.down, out hit,
           ((capsule.height / 2.0f) - capsule.radius) + stickToGroundDistance, 0, QueryTriggerInteraction.Ignore))
        {
            if (Mathf.Abs(Vector3.Angle(hit.normal, Vector3.up)) < 85)
            {
                rigidbody.velocity = Vector3.ProjectOnPlane(rigidbody.velocity, hit.normal);
            }
        }
    }

    void GroundCheck()
    {
        previouslyGrounded = isGrounded;

        RaycastHit hit;

        //raycast down to see if ground is there
        if (Physics.SphereCast(transform.position, capsule.radius * (1.0f - capsuleOffset), Vector3.down, out hit,
            ((capsule.height / 2.0f) - capsule.radius) + groundCheckDistance, ~0, QueryTriggerInteraction.Ignore))
        {

            //if ground is there, set appropriate bools to true
            isGrounded = true;
            groundContactNormal = hit.normal;
        }
        else
        {
            //otherwise set the appropriate bools to false
            isGrounded = false;
            groundContactNormal = Vector3.up;
        }


        if (!previouslyGrounded && isGrounded && jumping)
            jumping = false;

        addedForce = false;
    }
}
