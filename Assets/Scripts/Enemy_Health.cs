﻿using UnityEngine;
using System.Collections;

public class Enemy_Health : MonoBehaviour
{
    [Header("Health")]
    public float health;

    [Header("Drop")]
    public GameObject drop;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        CheckIfAlive();
    }

    public void TakeDamage(float dmg)
    {
        health -= dmg;
    }

    void CheckIfAlive()
    {
        if (health <= 0)
        {
            if (RandomChance() > 8)
            {
                GameObject newDrop = Instantiate(drop) as GameObject;
                newDrop.transform.position = transform.position;
            }
            Destroy(gameObject);
        }
    }

    float RandomChance()
    {
        return Random.Range(0, 10);
    }
}
