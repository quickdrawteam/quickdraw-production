﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Player_ShootManager : MonoBehaviour {

    public GameObject activeWeapon = null;      // The gun that the player is currently using
    public GameObject primaryWeapon = null;     // The players main weapon
    public GameObject secondaryWeapon = null;   // The players back-up weapon
    public float interactDelay = 0.5f;          // How long the player will need to hold down the interact button
    public KeyCode interactKey = KeyCode.F; 

    private bool weaponActive = false;
    private bool UISetup = false;
    private float interactTimer;



    [Header("UI")]
    public Text bulletsText;   
    public Image outerReticle; 
    private Canvas canvas;
    private Vector3 outerReticleScaleInit;

    // Use this for initialization
    void Start ()
    {
        interactTimer = interactDelay;
        
        canvas = GetComponent<Player_Health>().canvas;
    }

    void SetupUI()
    {
        if (canvas == null)
            canvas = GetComponent<Player_Health>().canvas;
        if (canvas != null)
        {
            if (bulletsText == null)
                bulletsText = canvas.transform.FindChild("bulletsText").GetComponent<Text>();

            if (outerReticle == null)
            {
                outerReticle = canvas.transform.FindChild("outerReticle").GetComponent<Image>();
                outerReticleScaleInit = outerReticle.transform.localScale;
            }

            UISetup = true;
        }
        else
            UISetup = false;
    }

    void WeaponPickup()
    {
        activeWeapon.SendMessage("Deactivate");
        weaponActive = false;
    }

    void WeaponSwitch()
    {
        activeWeapon.SendMessage("Deactivate");
        weaponActive = false;

        if (activeWeapon.gameObject == primaryWeapon.gameObject)
            activeWeapon = secondaryWeapon;
        else if (activeWeapon.gameObject == secondaryWeapon.gameObject)
            activeWeapon = secondaryWeapon;
    }

    void UpdateUI()
    {

    }
	
	// Update is called once per frame
	void Update ()
    {
        if (activeWeapon == null) return;

        if (UISetup == false)
            SetupUI();

        if (weaponActive == false)
        {
            activeWeapon.SendMessage("Activate");
            weaponActive = true;
        }


	}


}
