﻿using UnityEngine;
using System.Collections;

public class Gun_Script : MonoBehaviour
{
    #region Variables
    #region Bullet Settings
    [Header("Bullet Settings")]
    protected int initBulletDamage;
    protected float initbulletSpread;   //How large the cone of fire is
    public float bulletSpread;       //How large the cone of fire is
    public float ADS_Spread;         //How large the cone of fire is
    public float bulletRange;        //How far each bullet can go 
    public float reloadSpeed;        //How long it takes to reload 
    public float bulletsPerShot;     //How many bullets were shot at once
    public float fireSpeed;          //Time between shots

    public int bulletDamage;       //How much damage each bullet does on impact
    public int magSize;            //The max amount of bullets in a magazine
    public int maxAmmo;            //The max amount of bullets the player can carry at any given time


    protected bool reloading;          //Keeping track if reloading or not
    protected int bulletsInMag;       //Tracks the bullets in the current magazine 
    protected int ammoCount;          //Tracks the total amount of bullets the player is carrying

    protected float shootTimer;         //Time it takes to shoot
    protected float reloadTimer;        //Tracks how long it takes to reload
    #endregion
    #region BulletHole
    [Header("BulletHole")]
    public Sprite[] bulletHoles;        //What the bullet holes look like
    public GameObject bulletHoleObject;   //When a static object is shot this image is instantiated at the point where it hits
    #endregion
    #region Tracers
    [Header("Tracers")]
    public GameObject tracerObject;             //The Tracers rigidBody that will get forced from bulletOrigin
    public GameObject bulletOrigin;       //Where the bullets come from ( Usually tip of gun)
    public float tracerSpeed;        //How fast the tracers move in worldspace after fired
    #endregion
    #region Misc Variables
    [HideInInspector]
    public bool active;
    protected Camera activePlayerCamera;
    #endregion
    #endregion

    #region Functions
    // Use this for initialization
    virtual public void Start()
    {
        reloading = false;
        activePlayerCamera = FindActiveCamera();
        active = true;
        bulletsInMag = magSize;
        ammoCount = maxAmmo;
        reloadTimer = reloadSpeed;
        initBulletDamage = bulletDamage;
    }
    
    virtual public void Update()    {}
    virtual public void Shoot()     {}

    public void ShootTracer(Vector3 direction)
    {
        Vector3 origin = bulletOrigin.transform.position;
        GameObject tracer = Instantiate(tracerObject, origin + direction, FindActiveCamera().transform.rotation) as GameObject;
        tracer.GetComponent<Rigidbody>().AddForce(direction * tracerSpeed);
        Destroy(tracer, 1);
    }

    public void Reload()
    {
        //if you have bullets to reload with
        if (ammoCount > 0)
        {
            //reload
            if (reloadTimer <= 0)
            {
                reloadTimer = reloadSpeed;
                ammoCount -= magSize;
                if (bulletsInMag > 0)
                    ammoCount += bulletsInMag;

                bulletsInMag = magSize;

                if (ammoCount < 0)
                {
                    bulletsInMag += ammoCount;
                    ammoCount -= ammoCount;

                }
                reloading = false;
            }
            reloadTimer -= Time.deltaTime;
        }
        else
        {
            //display message here telling them to find a new weapon
            //Debug.Log("Find a new weapon");
            reloading = false;
        }
    }

    public void CreateBulletHole(RaycastHit hitInfo)
    {
        var hitRotation = Quaternion.LookRotation(-hitInfo.normal);
        GameObject newBulletHole = Instantiate(bulletHoleObject, hitInfo.point, hitRotation) as GameObject;
        newBulletHole.transform.position += newBulletHole.transform.forward * -0.001f;
        newBulletHole.GetComponent<SpriteRenderer>().sprite = bulletHoles[0];
        newBulletHole.transform.parent = GameObject.Find("BulletHolesHolder").gameObject.transform;
    }

    public Camera FindActiveCamera()
    {
        Camera[] cameras = FindObjectsOfType<Camera>();
        for (int i = 0; i < cameras.Length; ++i)
        {
            if (cameras[i].enabled == true && cameras[i].tag == "PlayerCamera")
                return cameras[i];

        }
        return null;
    }

    public bool GetActive()
    {
        return active;
    }

    public void ChangeActive()
    {
        active = !active;
    }
    #endregion
}
