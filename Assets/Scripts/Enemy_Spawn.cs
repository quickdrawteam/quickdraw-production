﻿using UnityEngine;
using System.Collections;

public class Enemy_Spawn : MonoBehaviour
{
    public GameObject playerTarget;
    public GameObject enemyToSpawn = null;
    public float spawnDelay = 1.0f;
    public bool spawnEnemies = false;
    public int spawnAmount = 1;

    private GameObject[] players;
    private float spawnTimer = 0;
        void Start()
    {
        spawnTimer = spawnDelay;
    }

    void Update()
    {
        players = GameObject.FindGameObjectsWithTag("Player");
        if (players.Length == 2)
        {
            if (Vector3.Distance(players[0].transform.position, transform.position) < Vector3.Distance(players[1].transform.position, transform.position))
            {
                playerTarget = players[0];
            }
            else
            {
                playerTarget = players[1];
            }
        }

        if (players.Length == 1)
        {
            playerTarget = players[0].gameObject;
        }

        if (spawnEnemies)
        {
            spawnTimer -= Time.deltaTime;
            if (spawnTimer <= 0)
            {
                SpawnEnemy(spawnAmount);
                spawnTimer = spawnDelay;
                spawnEnemies = false;
            }
        }
    }

    void SpawnEnemy(int spawnCount)
    {
        for (int i = 0; i < spawnCount; ++i)
        {
            GameObject enemy = Instantiate(enemyToSpawn, transform.position, transform.rotation) as GameObject;
            enemy.transform.position += new Vector3(Random.Range(0, 10), Random.Range(0, 10), Random.Range(0, 10));
            enemy.GetComponent<Enemy_AI>().player = playerTarget;
            enemy.transform.parent = GameObject.Find("EnemiesHolder").transform;
        }
    }
}
