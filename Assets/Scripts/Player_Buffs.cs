﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class Player_Buffs : MonoBehaviour
{

    public enum SelectedBuff
    {
        NONE = 0,           // Buff id of 0
        MOVEMENT,           // Buff id of 1
        VISION,             // Buff id of 2
        DAMAGE,             // Buff id of 3
        HEALTH,             // Buff id of 4
        SPAWN_RATE,         // Buff id of 5
        INVERT_CONTROLS,    // Buff id of 6
        EXPLOSIVE_BULLETS,  // Buff id of 7
        REDUCE_AMMO         // Buff id of 8
    }


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void BuffPlayer(GameObject player, SelectedBuff buff, float value)
    {
        switch (buff)
        {
            case SelectedBuff.NONE:
                break;
            case SelectedBuff.MOVEMENT:
                player.GetComponent<Player_Movement>().moveSpeed += (int)value;
                break;
            case SelectedBuff.VISION:
                player.transform.FindChild("Camera").GetComponent<VignetteAndChromaticAberration>().intensity *= value;
                break;
            case SelectedBuff.DAMAGE:
                player.GetComponent<Player_Shoot>().bulletDamage += (int)value;
                break;
            case SelectedBuff.HEALTH:
                player.GetComponent<Player_Health>().maxHealth += (int)value;
                break;
            case SelectedBuff.SPAWN_RATE:

                break;
            case SelectedBuff.INVERT_CONTROLS:

                break;
            case SelectedBuff.EXPLOSIVE_BULLETS:

                break;
            case SelectedBuff.REDUCE_AMMO:
                player.GetComponent<Player_Shoot>().SetAmmoCount((int)value);
                break;
            default:
                break;
        }
    }

    void BuffPlayer(GameObject player, SelectedBuff buff, bool activate)
    {

    }

    public void ChooseBuff(GameObject player, int buffID, float value)
    {
        SelectedBuff buff = SelectedBuff.NONE;

        switch (buffID)
        {
            case 0:
                {
                    buff = SelectedBuff.NONE;
                    break;
                }
            case 1:
                {
                    buff = SelectedBuff.MOVEMENT;
                    break;
                }
            case 2:
                {
                    buff = SelectedBuff.VISION;
                    break;
                }
            case 3:
                {
                    buff = SelectedBuff.DAMAGE;
                    break;
                }
            case 4:
                {
                    buff = SelectedBuff.HEALTH;
                    break;
                }
            case 5:
                {
                    buff = SelectedBuff.SPAWN_RATE;
                    break;
                }
            case 6:
                {
                    buff = SelectedBuff.INVERT_CONTROLS;
                    break;
                }
            case 7:
                {
                    buff = SelectedBuff.EXPLOSIVE_BULLETS;
                    break;
                }
            case 8:
                {
                    buff = SelectedBuff.REDUCE_AMMO;
                    break;
                }
            default:
                buff = SelectedBuff.NONE;
                break;
        }

        BuffPlayer(player, buff, value);
    }
}
