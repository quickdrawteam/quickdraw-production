﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Player_NetworkManager : NetworkBehaviour
{
    
    //called from client to tell the server that a player has been damaged
    [Command]
    public void CmdDamagePlayer(NetworkInstanceId ID, float damage)
    {
        //then relay that message to all clients telling them that the object has taken damage
        GameObject target = NetworkServer.FindLocalObject(ID);
        target.GetComponent<Player_NetworkManager>().RpcTakeDamage(damage);
    }

    //sends the updated health to all the clients
    [ClientRpc]
    void RpcTakeDamage(float damage)
    {
        GetComponent<Player_Health>().TakeDamage(damage);
    }


    // Player buff server update
    [Command]
    public void CmdUpdateBuffs(NetworkInstanceId ID, int buffID, float value)
    {
        GameObject target = NetworkServer.FindLocalObject(ID);
        target.GetComponent<Player_NetworkManager>().RpcUpdateBuffs(target, buffID, value);
    }

    [ClientRpc]
    public void RpcUpdateBuffs(GameObject target, int buffID, float value)
    {
        GetComponent<Player_Buffs>().ChooseBuff(target, buffID, value);
    }
}
