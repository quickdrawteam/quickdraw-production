﻿using UnityEngine;
using System.Collections;

public class Rifle_Script : Gun_Script
{
    #region Variables

    #endregion

    #region Functions
    // Update is called once per frame
    override public void Update()
    {
        if (active)
        {
            #region Aiming Down Sights Manipulator
            if (Input.GetMouseButton(1))
                bulletSpread = ADS_Spread;
            else
                bulletSpread = initbulletSpread;
            #endregion

            #region Shooting ( Checking to see if can Shoot)
            //If the player can shoo and the player has pressed the shoot button
            if (shootTimer <= 0 && Input.GetMouseButton(0) && !reloading && bulletsInMag > 0)
            {
                Debug.Log("Charging");
                //Shoot and reset the delay
                for (int i = 0; i < bulletsPerShot; ++i)
                    Shoot();
                shootTimer = fireSpeed;
            }

            if (shootTimer > 0)
                shootTimer -= Time.deltaTime;
            #endregion

            #region Reloading
            if (bulletsInMag <= 0)
                reloading = true;
            else if (bulletsInMag < magSize && Input.GetKeyDown(KeyCode.R))
                reloading = true;

            if (reloading)
                Reload();
            #endregion
        }
    }

    override public void Shoot()
    {
        //Everytime Shoot is called, one bullet is taken out of the mag
        bulletsInMag -= 1;

        //Makes random radius with given bulletspread ( In meters )
        float randomRadius = Random.Range(0, bulletSpread);
        //Makes random angle from player
        float randomAngle = Random.Range(0, 2 * Mathf.PI);

        //Calculate raycast direction
        Vector3 direction = new Vector3(randomRadius * Mathf.Cos(randomAngle), randomRadius * Mathf.Sin(randomAngle), bulletRange);

        direction = activePlayerCamera.transform.TransformDirection(direction.normalized);

        //Raycast
        Ray r = new Ray(activePlayerCamera.transform.position, direction);
        RaycastHit hit;

        //Shoots tracer in the same direction as the bullet
        ShootTracer(direction);

        if (Physics.Raycast(r, out hit))
        {
            //For debug only 9 So we can see bullet)
            Debug.DrawLine(transform.position, hit.point);

            if (hit.collider.gameObject.tag == "Enemy")
            {
                //tell the enemy to take damage depending on the players damage
                hit.collider.gameObject.GetComponent<Enemy_Health>().TakeDamage(bulletDamage / bulletsPerShot);
            }
            else if (hit.collider.gameObject.tag == "Player" && hit.collider.gameObject != this.gameObject)
            {
                GetComponent<Player_NetworkManager>().CmdDamagePlayer(hit.collider.GetComponent<Player_NetworkManager>().netId, bulletDamage);

                //syntax for networking buffs (below)
                //GetComponent<Player_NetworkManager>().CmdUpdateBuffs(hit.collider.GetComponent<Player_NetworkManager>().netId, 3, -50.0f);
            }
            else if (hit.collider.gameObject.isStatic)
            {
                CreateBulletHole(hit);
            }
        }
    }
    #endregion
}
