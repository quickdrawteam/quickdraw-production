﻿using UnityEngine;
using System.Collections;

public class Enemy_AI : MonoBehaviour
{
    #region Variables
    //Private Variables
    private NavMeshAgent navMesh;        //The navmesh agent that moves the player around the terrain
    private Vector3 targetLocation; //Where the navmesh wants to set its destination to ( usually the players position)

    //Finite State Machine
    public State currentState;

    public enum State
    {
        SEEKING,
        IDLE,
        WANDER,
        SHOOTING,
        FLEEING,
    }

    //Timers
    private float canSeePlayerTimer;        //Counts up to 0.5 to add delay on searching for player
    private float wanderTimer;              //Adds a delay before getting a new wander target location
    private float shootingTimer = 2;

    //Public Variables
    public GameObject player;                    //The main player in the scene
    [Header("Enemy Movement Options")]
    public float movementSpeed;             //How fast the enemy moves across the terrain
    public float wanderRadius;

    [Header("General Enemy Settings")]
    public float detectionRadius;

    [Header("Enemy Shooting Settings")]
    public float timeBetweenShots;
    private float recentlyShot;
    public float bulletSpread;
    public float bulletRange;
    public int bulletDamage;
    public float reloadSpeed;
    [Tooltip("Time Between Shots")]
    public float fireSpeed;
    [Tooltip("Determines amount of bullets to be fired when the player shoots")]
    public float bulletsPerShot;
    public GameObject raycastOrigin = null;

    public GameObject bulletOrigin;

    private float shootTimer = 0;
    private float rayCastCounter = 0;

    [Header("Tracers")]
    //public Rigidbody tracer;
    public GameObject tracerBullet;
    public Transform tracerSpawnPoint;
    [Space(10)]
    public float tracerSpeed;
    #endregion

    #region Functions
    void Start()
    {
        navMesh = gameObject.GetComponent<NavMeshAgent>();
        currentState = State.WANDER;
        navMesh.speed = movementSpeed;
        recentlyShot = timeBetweenShots;
    }

    void FixedUpdate()
    {
        // if (player == null) return;
        CheckForChangeState();
        UpdateState();
        navMesh.Resume();
    }

    void UpdateState()
    {
        switch (currentState)
        {
            case State.SEEKING:
                SeekPlayer();
                break;
            case State.IDLE:
                Idle();
                break;
            case State.WANDER:
                Wander();
                break;
            case State.SHOOTING:
                Shooting();
                break;
            case State.FLEEING:
                Flee();
                break;
            default:
                break;
        }
    }

    void CheckForChangeState()
    {
        #region Seeking & Shooting
        //If can see player, seekforplayer
        if (Vector3.Distance(transform.position, player.transform.position) < detectionRadius)
        {
            //Raycast to see if you can see player
            RaycastHit hitInfo;
            if (Physics.Raycast(transform.position, (player.transform.position - transform.position), out hitInfo))
            {
                if (hitInfo.transform.gameObject == player)
                {
                    recentlyShot += Time.deltaTime;
                    if (recentlyShot >= timeBetweenShots)
                    {
                        ChangeState(State.SHOOTING);
                        recentlyShot = 0;
                        Invoke("ChangeStateSeeking", 0.5f);
                    }
                }
                else
                {
                    ChangeState(State.WANDER);
                }
            }

        }
        #endregion

    }

    void ChangeStateSeeking()
    {
        currentState = State.SEEKING;
    }

    void ChangeState(State nextState)
    {
        currentState = nextState;
    }

    public static Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask)
    {
        Vector3 randDirection = Random.insideUnitSphere * dist;

        randDirection += origin;

        NavMeshHit navHit;

        NavMesh.SamplePosition(randDirection, out navHit, dist, layermask);

        return navHit.position;
    }


    //State Functions
    void SeekPlayer()
    {
        if (Vector3.Distance(transform.position, player.transform.position) < 8)
        {
            navMesh.Stop();
        }
        else
        {
            navMesh.SetDestination(player.transform.position);
        }
    }

    void Idle()
    {
        navMesh.Stop();
        //Start idle animation
    }

    void Flee()
    {
        if (SearchForCover() != null)
        {
            navMesh.SetDestination(SearchForCover().position);
        }
        else
        {
            Vector3 directionToPlayer = transform.position - player.transform.position;
            navMesh.SetDestination(directionToPlayer);
            //Play fleeing animation
            //Cant shoot
        }
    }

    Transform SearchForCover()
    {
        return null;
        //Find the closest node tagged "Cover"
    }

    void Wander()
    {
        wanderTimer += Time.deltaTime;

        if (wanderTimer >= 3)
        {
            Vector3 newPos = RandomNavSphere(transform.position, wanderRadius, -1);
            navMesh.SetDestination(newPos);
            wanderTimer = 0;
        }
    }

    void Shooting()
    {
        navMesh.SetDestination(transform.position);
        if (gameObject.GetComponent<Enemy_Health>().health >= 0)
        {
            if (rayCastCounter <= 0)
            {
                //checks to see if the enemy can see the player
                rayCastCounter = fireSpeed;

                Vector3 direction = player.transform.position - raycastOrigin.transform.position;

                Ray r = new Ray(raycastOrigin.transform.position, direction);
                RaycastHit hit;

                if (Physics.Raycast(r, out hit))
                {
                    //if the player is seen
                    if (hit.collider.gameObject.tag == "Player" || hit.collider)
                    {
                        Debug.DrawLine(raycastOrigin.transform.position, hit.point);
                        ShootBurst();
                        //ShootRay();
                    }
                    else
                    {
                        //Debug.Log(hit.collider.gameObject.name);
                    }
                }
                else
                {
                    //move to new location
                    for (int i = 0; i < 30; ++i)
                        Debug.DrawLine(raycastOrigin.transform.position, hit.point);
                }
            }
            //stops the enemy raycasting every frame to help performance
            if (rayCastCounter > 0)
                rayCastCounter -= Time.deltaTime;
        }
    }

    void ShootRay()
    {
        transform.LookAt(player.transform.position);                      //look at the player before shooting forwards


        float randomRadius = Random.Range(0, bulletSpread);                                     // ray casts will be in a circle area
        float randomAngle = Random.Range(0, 2 * Mathf.PI);                                      // ray casts will be in a circle area


        Vector3 direction = new Vector3(randomRadius * Mathf.Cos(randomAngle),
                            randomRadius * Mathf.Sin(randomAngle), bulletRange);                //Calculate raycast direction

        direction = bulletOrigin.transform.TransformDirection(direction.normalized);            //make direction match the transform

        // eg. converting the vector3 forward to transform forward

        Ray r = new Ray(raycastOrigin.transform.position, direction);                           //raycast to the player
        RaycastHit hit;

        if ((transform.position - GameObject.FindGameObjectWithTag("Player").gameObject.transform.position).magnitude < 20)
        {
            if (Physics.Raycast(r, out hit))
            {
                if (hit.collider.gameObject.tag == "Player")                                    //if the object hit is the player
                {
                    hit.collider.gameObject.GetComponent<Player_Health>().TakeDamage(bulletDamage);    //tell the player to take damage depending on the enemies damage
                    ShootTracer(direction);                                                            //Shoot visible bullet
                    Debug.DrawRay(transform.position, direction);
                }
            }
        }
    }

    void ShootTracer(Vector3 direction)
    {
        Vector3 origin = bulletOrigin.transform.position;
        GameObject tracer = Instantiate(tracerBullet, origin + direction, transform.rotation) as GameObject;
        tracer.GetComponent<Rigidbody>().AddForce(direction * tracerSpeed);
        Destroy(tracer, 1);
    }

    void ShootBurst()
    {
        Invoke("ShootRay", 0);
        Invoke("ShootRay", 0.1f);
        Invoke("ShootRay", 0.2f);
    }
    #endregion
}
