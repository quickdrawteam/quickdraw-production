﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class Player_Health : MonoBehaviour
{
    [Header("Health")]
    public float currentHealth;     //How much health the player currently with 
    public float maxHealth;

    public bool _isLocalPlayer = false;
    public Canvas canvas;

    public Text healthText; 
	// Use this for initialization
	void Start ()
    {
        currentHealth = maxHealth;
        healthText = null;


        if(GetComponent<SetupLocalPlayer>().CheckIfLocalPlayer())
            canvas = Instantiate(canvas);
	}
	
	// Update is called once per frame
	void Update ()
    {
        UpdateText();
        CheckIfAlive();
	}

    void SetCanvas(Canvas newCanvas)
    {
        canvas = newCanvas;
    }

    void UpdateText()
    {
        if (healthText == null)
            healthText = canvas.transform.FindChild("healthText").GetComponent<Text>();


        if(healthText != null)
            healthText.text = currentHealth.ToString();
    }

    public void TakeDamage(float damage)
    {
        currentHealth -= damage;
        healthText.text = currentHealth.ToString();

        if (currentHealth <= 0 )
        {
            currentHealth = 0;
        }

    }

    public void AddHealth(float _health)
    {
        currentHealth += _health;
        if (currentHealth >= maxHealth)
        {
            currentHealth = maxHealth;
        }
    }

    public void DamageEnemyPlayer(float damage)
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        for(int i = 0; i < players.Length; ++i)
        {
            if(players[i].gameObject != this.gameObject)
            {
                players[i].GetComponent<Player_Health>().TakeDamage(damage);
            }
        }
    }

    void CheckIfAlive()
    {
        if (currentHealth <= 0)
        {
            Debug.Log("Dead");
        }
    }

    void SetupCanvas(GameObject canvas)
    {
        GameObject temp = Instantiate(canvas);
        temp.SetActive(true);
    }    
}
